/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.scrabble.modele;

import java.util.ArrayList;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;

/**
 *
 * @author benjamin
 */
public class MotTest extends TestCase {
    
    public MotTest(String testName) {
        super(testName);
    }
    
    public void testParsing()
    {
        Mot mot = new Mot("BABAR");
        ArrayList<Lettre> lettres = mot.getLettres();
        
        assertEquals(Lettre.B, lettres.get(0));
        assertEquals(Lettre.A, lettres.get(1));
        assertEquals(Lettre.B, lettres.get(2));
        assertEquals(Lettre.A, lettres.get(3));
        assertEquals(Lettre.R, lettres.get(4));
        assertEquals("BABAR",mot.toString());
    }
}
