/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.scrabble.plateau;

import fr.epsi.scrabble.modele.Lettre;
import junit.framework.TestCase;

/**
 *
 * @author benjamin
 */
public class CaseTest extends TestCase {
    
    public CaseTest(String testName) {
        super(testName);
    }

    /**
     * Test of getTypeCase method, of class Case.
     */
    public void testGetTypeCase() {
        System.out.println("getTypeCase");
        Case instance = new Case(TypeCase.CASE_ETOILE,0);
        TypeCase expResult = TypeCase.CASE_ETOILE;
        TypeCase result = instance.getTypeCase();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLettre method, of class Case.
     */
    public void testGetLettre() {
        System.out.println("getLettre");
        Case instance = new Case(TypeCase.BLEU_CLAIRE,0);
        Lettre expResult = null;
        Lettre result = instance.getLettre();
        assertEquals(expResult, result);
        
        System.out.println("setLettre");
        Lettre l_lettre = Lettre.B;
        instance.setLettre(l_lettre);
        expResult = l_lettre;
        result = instance.getLettre();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumeroCase method, of class Case.
     */
    public void testGetNumeroCase() {
        System.out.println("getNumeroCase");
        Case instance = new Case(TypeCase.BLEU_CLAIRE,0);
        int expResult = 0;
        int result = instance.getNumeroCase();
        assertEquals(expResult, result);
    }
}
