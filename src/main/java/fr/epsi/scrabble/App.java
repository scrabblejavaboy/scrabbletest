package fr.epsi.scrabble;

import fr.epsi.scrabble.database.H2ConnectionDB;
import fr.epsi.scrabble.dictionnaire.Dictionnaire;
import fr.epsi.scrabble.ihm.Main;
import fr.epsi.scrabble.plateau.Grille;
import javax.swing.UIManager;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) throws Exception {
        try {
            //uniformisation du style
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

            //Chargepment de la grille
            Grille grille = Grille.getGrille();

            // On charge le dictionnaire
            H2ConnectionDB l_connexion = new H2ConnectionDB();
            Dictionnaire l_dico = Dictionnaire.getInstance();

            l_dico.addMots(l_connexion.selectAllMots());
            
            //Lancement de l'IHM
            Main l_main = new Main();
            
            //Centre la fenetre
            l_main.setLocationRelativeTo(null);

        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }
}
