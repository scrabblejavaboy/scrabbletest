package fr.epsi.scrabble.plateau;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author benjamin
 */
public class Ligne implements Serializable{
    
    private ArrayList<Case> listeCase;
    private int numeroLigne;
    
    public Ligne(ArrayList m_listeCase) {
        this.listeCase = m_listeCase;
    }
    
    public void addCase(Case _case) throws Exception {
        try {
            int l_numeroCase = this.listeCase.size() + 1;
            _case.setNumeroCase(l_numeroCase);
            this.listeCase.add(_case);
        } catch (Exception ex) {
            throw new Exception("Erreur lors de l'ajout d'une case : " + ex.getMessage().toString());
        }
    }
    
    /**
     * Retourne la liste des cases
     * @return ArrayList de case
     */
    public ArrayList<Case> getCases()
    {
        return this.listeCase;
    }
    
    /**
     * Retourne une case précise
     * @param _id Id de la case
     * @return Case
     * @throws Exception 
     */
    public Case getCase(int _id) throws Exception {
        try {
            return this.listeCase.get(_id);
        } catch (Exception ex) {
            throw new Exception("Erreur lors de la lecture d'une case : " + ex.getMessage().toString());
        }
    }

    /**
     * Retourne si la ligne possède une lettre ou non
     *
     * @return Vrai si elle possède une lettre
     */
    public boolean possedeLettre() {
        try {
            boolean l_possedeLettre = false;
            for (Case _case : this.listeCase) {
                if (_case.getLettre() != null) {
                    l_possedeLettre = true;
                    break;
                }
            }
            return l_possedeLettre;
        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * Retourne un tableau a deux dimensions contenant les coordonnées des
     * lettres (x,y)
     *
     * @return Tableau a deux dimensions
     */
    public ArrayList<int[]> getCoordonneesLettre() {
        try {
            ArrayList l_coordonnees = new ArrayList();
            
            for (Case _case : this.listeCase) {
                if (_case.getLettre() != null) {
                    l_coordonnees.add(new int[]{this.numeroLigne, _case.getNumeroCase()});
                    break;
                }
            }
            
            return l_coordonnees;
        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * Définis le numéro de la ligne
     *
     * @param _numLigne
     */
    public void setNumeroLigne(int _numLigne) {
        this.numeroLigne = _numLigne;
    }
}
