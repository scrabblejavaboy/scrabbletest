package fr.epsi.scrabble.plateau;

/**
 * @author benjamin
 */
public enum TypeMultiplieur {

    WORD,
    LETTER,
    NOTHING;
}
