package fr.epsi.scrabble.plateau;

import fr.epsi.scrabble.modele.Lettre;
import java.io.Serializable;

/**
 * @author benjamin
 */
public class Case implements Serializable {

    private TypeCase typeCase;
    private Lettre lettre;
    private int numeroCase;

    /**
     * Constructeur
     *
     * @param typeCase
     * @param numeroCase
     */
    public Case(TypeCase typeCase, int numeroCase) {
        this.typeCase = typeCase;
        this.lettre = null;
        this.numeroCase = numeroCase;
    }

    /**
     * Retourne le type de la case
     *
     * @return
     */
    public TypeCase getTypeCase() {
        return typeCase;
    }

    /**
     * Retourne la lettre
     *
     * @return
     */
    public Lettre getLettre() {
        return lettre;
    }

    /**
     * Retourne la lettre sous forme de String
     *
     * @return
     */
    public String getValue() {
        String l_value;

        if (this.lettre == null) {
            l_value = "";
        } else {
            l_value = this.lettre.toString();
        }

        return l_value;
    }

    /**
     * Definis la lettre
     *
     * @param lettre
     */
    public void setLettre(Lettre lettre) {
        this.lettre = lettre;
    }

    /**
     * Retourne le numéro de la case
     *
     * @return
     */
    public int getNumeroCase() {
        return numeroCase;
    }

    /**
     * Definis le numéro de la case
     *
     * @param numeroCase
     */
    public void setNumeroCase(int numeroCase) {
        this.numeroCase = numeroCase;
    }

    /**
     * Retourne le score
     * @return 
     */
    public int getScore() {
        int score = 0;

        if (this.lettre != null) {
            score = this.lettre.getValeur();

            if (this.typeCase.getTypeMultiplieur().equals(TypeMultiplieur.LETTER)) {
                score *= this.typeCase.getMultiplieur();
            }
        }

        return score;
    }
}
