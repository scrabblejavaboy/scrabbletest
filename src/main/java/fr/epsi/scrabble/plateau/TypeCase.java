package fr.epsi.scrabble.plateau;

import java.awt.Color;

/**
 * @author benjamin
 */
public enum TypeCase {

    ROUGE {
        public Color getColor() {
            return new Color(255,0,0);
        }

        public TypeMultiplieur getTypeMultiplieur() {
            return TypeMultiplieur.WORD;
        }

        public int getMultiplieur() {
            return 3;
        }
    },
    ROSE {
        public Color getColor() {
            return new Color(255,100,255);
        }

        public TypeMultiplieur getTypeMultiplieur() {
            return TypeMultiplieur.WORD;
        }

        public int getMultiplieur() {
            return 2;
        }
    },
    BLEU_FONCE {
        public Color getColor() {
            return new Color(0,20,255);
        }

        public TypeMultiplieur getTypeMultiplieur() {
            return TypeMultiplieur.LETTER;
        }

        public int getMultiplieur() {
            return 3;
        }
    },
    BLEU_CLAIRE {
        public Color getColor() {
            return new Color(70,170,255);
        }

        public TypeMultiplieur getTypeMultiplieur() {
            return TypeMultiplieur.LETTER;
        }

        public int getMultiplieur() {
            return 2;
        }
    },
    CASE_ETOILE {
        public Color getColor() {
            return new Color(255,100,255);
        }

        public TypeMultiplieur getTypeMultiplieur() {
            return TypeMultiplieur.NOTHING;
        }

        public int getMultiplieur() {
            return 1;
        }
    },
    NEUTRE {
        public Color getColor() {
            return Color.WHITE;
        }

        public TypeMultiplieur getTypeMultiplieur() {
            return TypeMultiplieur.NOTHING;
        }

        public int getMultiplieur() {
            return 1;
        }
    };
    
    public abstract Color getColor();
    public abstract TypeMultiplieur getTypeMultiplieur();
    public abstract int getMultiplieur();
}
