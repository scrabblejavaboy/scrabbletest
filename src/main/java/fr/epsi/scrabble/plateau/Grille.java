package fr.epsi.scrabble.plateau;

import fr.epsi.scrabble.joueur.Mouvement;
import fr.epsi.scrabble.modele.Lettre;
import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * @author benjamin
 */
public class Grille implements Serializable {

    private ArrayList<Ligne> listeLigne;
    private static Grille instance;
    private int score;

    /**
     * Constructeur
     */
    private Grille() {
        this.listeLigne = new ArrayList<>();

        this.init();
    }

    /**
     * Singleton
     *
     * @return Grille
     */
    public static Grille getGrille() {
        if (instance == null) {
            instance = new Grille();
        }
        return instance;
    }

    /**
     * Créer une nouvelle grille
     *
     * @return Grille
     */
    public static Grille newGrille() {
        instance = new Grille();
        return instance;
    }

    /**
     * Créer une grille a partir d'une grille
     *
     * @param grille
     * @return
     */
    public static Grille newGrille(Grille grille) {
        instance = grille;
        return instance;
    }

    /**
     * Retourne la liste des lignes
     *
     * @return ArrayList
     */
    public ArrayList<Ligne> getLignes() {
        return this.listeLigne;
    }

    /**
     * Retourne une ligne
     *
     * @param _id
     * @return
     */
    public Ligne getLigne(int _id) {
        return this.listeLigne.get(_id);
    }

    /**
     * Retourne la lettre d'une case 0,0 retourne la premiere case de la
     * premiere colonne
     *
     * @param x Ligne
     * @param y Colonne
     * @return Lettre
     * @throws java.lang.Exception
     */
    public Lettre getLettre(int x, int y) throws Exception {
        try {
            Lettre lettre = null;

            if (x >= 0 && y >= 0 && x < 14 && y < 14) {
                lettre = this.listeLigne.get(y).getCase(x).getLettre();
            }

            return lettre;
        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * Retourne une case
     *
     * @param x Ligne
     * @param y Colonne
     * @return
     */
    public Case getCase(int x, int y) throws Exception {
        try {
            return this.listeLigne.get(y).getCase(x);
        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * Retourne vrai si la grille contient une lettre
     *
     * @return
     */
    public boolean possedeLettre() {
        boolean possedeLettre = false;
        for (Ligne ligne : this.listeLigne) {
            if (ligne.possedeLettre()) {
                possedeLettre = true;
                break;
            }
        }

        return possedeLettre;
    }

    /**
     * Retourne le score
     *
     * @return
     */
    public int getScore() {
        return score;
    }

    /**
     * Definis la lettre d'une case
     *
     * @param x
     * @param y
     * @param lettre
     * @throws Exception
     */
    public void setLettre(int x, int y, Lettre lettre) throws Exception {
        try {
            this.listeLigne.get(y).getCase(x).setLettre(lettre);
        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * Ajoute une ligne à la grille
     *
     * @param _ligne
     */
    public void addLigne(Ligne _ligne) {
        try {
            int l_numLigne = this.listeLigne.size() + 1;

            _ligne.setNumeroLigne(l_numLigne);

            this.listeLigne.add(l_numLigne - 1, _ligne);
        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * Place un mouvement
     *
     * @param mvt
     */
    public void placeMouvement(Mouvement mvt) {
        try {
            Point p = mvt.getAncre();

            for (Lettre lettre : mvt.getMot().getLettres()) {
                Case caseGrille;

                caseGrille = this.getCase(p.x, p.y);


                if (caseGrille.getLettre() == null) {
                    caseGrille.setLettre(lettre);
                }

                p = mvt.getNextPoint(p);
            }

            this.score += mvt.getScore();
        } catch (Exception ex) {
            Logger.getLogger(Grille.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Initialise la grille
     */
    private void init() {
        ArrayList<Case> l_ligne_1 = new ArrayList();

        l_ligne_1.add(new Case(TypeCase.ROUGE, 0));
        l_ligne_1.add(new Case(TypeCase.NEUTRE, 1));
        l_ligne_1.add(new Case(TypeCase.NEUTRE, 2));
        l_ligne_1.add(new Case(TypeCase.BLEU_CLAIRE, 3));
        l_ligne_1.add(new Case(TypeCase.NEUTRE, 4));
        l_ligne_1.add(new Case(TypeCase.NEUTRE, 5));
        l_ligne_1.add(new Case(TypeCase.NEUTRE, 6));
        l_ligne_1.add(new Case(TypeCase.ROUGE, 7));
        l_ligne_1.add(new Case(TypeCase.NEUTRE, 8));
        l_ligne_1.add(new Case(TypeCase.NEUTRE, 9));
        l_ligne_1.add(new Case(TypeCase.NEUTRE, 10));
        l_ligne_1.add(new Case(TypeCase.BLEU_CLAIRE, 11));
        l_ligne_1.add(new Case(TypeCase.NEUTRE, 12));
        l_ligne_1.add(new Case(TypeCase.NEUTRE, 13));
        l_ligne_1.add(new Case(TypeCase.ROUGE, 14));

        ArrayList<Case> l_ligne_2 = new ArrayList();

        l_ligne_2.add(new Case(TypeCase.NEUTRE, 0));
        l_ligne_2.add(new Case(TypeCase.ROSE, 1));
        l_ligne_2.add(new Case(TypeCase.NEUTRE, 2));
        l_ligne_2.add(new Case(TypeCase.NEUTRE, 3));
        l_ligne_2.add(new Case(TypeCase.NEUTRE, 4));
        l_ligne_2.add(new Case(TypeCase.BLEU_FONCE, 5));
        l_ligne_2.add(new Case(TypeCase.NEUTRE, 6));
        l_ligne_2.add(new Case(TypeCase.NEUTRE, 7));
        l_ligne_2.add(new Case(TypeCase.NEUTRE, 8));
        l_ligne_2.add(new Case(TypeCase.BLEU_FONCE, 9));
        l_ligne_2.add(new Case(TypeCase.NEUTRE, 10));
        l_ligne_2.add(new Case(TypeCase.NEUTRE, 11));
        l_ligne_2.add(new Case(TypeCase.NEUTRE, 12));
        l_ligne_2.add(new Case(TypeCase.ROSE, 13));
        l_ligne_2.add(new Case(TypeCase.NEUTRE, 14));

        ArrayList<Case> l_ligne_3 = new ArrayList();

        l_ligne_3.add(new Case(TypeCase.NEUTRE, 0));
        l_ligne_3.add(new Case(TypeCase.NEUTRE, 1));
        l_ligne_3.add(new Case(TypeCase.ROSE, 2));
        l_ligne_3.add(new Case(TypeCase.NEUTRE, 3));
        l_ligne_3.add(new Case(TypeCase.NEUTRE, 4));
        l_ligne_3.add(new Case(TypeCase.NEUTRE, 5));
        l_ligne_3.add(new Case(TypeCase.BLEU_CLAIRE, 6));
        l_ligne_3.add(new Case(TypeCase.NEUTRE, 7));
        l_ligne_3.add(new Case(TypeCase.BLEU_CLAIRE, 8));
        l_ligne_3.add(new Case(TypeCase.NEUTRE, 9));
        l_ligne_3.add(new Case(TypeCase.NEUTRE, 10));
        l_ligne_3.add(new Case(TypeCase.NEUTRE, 11));
        l_ligne_3.add(new Case(TypeCase.ROSE, 12));
        l_ligne_3.add(new Case(TypeCase.NEUTRE, 13));
        l_ligne_3.add(new Case(TypeCase.NEUTRE, 14));

        ArrayList<Case> l_ligne_4 = new ArrayList();

        l_ligne_4.add(new Case(TypeCase.BLEU_CLAIRE, 0));
        l_ligne_4.add(new Case(TypeCase.NEUTRE, 1));
        l_ligne_4.add(new Case(TypeCase.NEUTRE, 2));
        l_ligne_4.add(new Case(TypeCase.ROSE, 3));
        l_ligne_4.add(new Case(TypeCase.NEUTRE, 4));
        l_ligne_4.add(new Case(TypeCase.NEUTRE, 5));
        l_ligne_4.add(new Case(TypeCase.NEUTRE, 6));
        l_ligne_4.add(new Case(TypeCase.BLEU_CLAIRE, 7));
        l_ligne_4.add(new Case(TypeCase.NEUTRE, 8));
        l_ligne_4.add(new Case(TypeCase.NEUTRE, 9));
        l_ligne_4.add(new Case(TypeCase.NEUTRE, 10));
        l_ligne_4.add(new Case(TypeCase.ROSE, 11));
        l_ligne_4.add(new Case(TypeCase.NEUTRE, 12));
        l_ligne_4.add(new Case(TypeCase.NEUTRE, 13));
        l_ligne_4.add(new Case(TypeCase.BLEU_CLAIRE, 14));

        ArrayList<Case> l_ligne_5 = new ArrayList();

        l_ligne_5.add(new Case(TypeCase.NEUTRE, 0));
        l_ligne_5.add(new Case(TypeCase.NEUTRE, 1));
        l_ligne_5.add(new Case(TypeCase.NEUTRE, 2));
        l_ligne_5.add(new Case(TypeCase.NEUTRE, 3));
        l_ligne_5.add(new Case(TypeCase.ROSE, 4));
        l_ligne_5.add(new Case(TypeCase.NEUTRE, 5));
        l_ligne_5.add(new Case(TypeCase.NEUTRE, 6));
        l_ligne_5.add(new Case(TypeCase.NEUTRE, 7));
        l_ligne_5.add(new Case(TypeCase.NEUTRE, 8));
        l_ligne_5.add(new Case(TypeCase.NEUTRE, 9));
        l_ligne_5.add(new Case(TypeCase.ROSE, 10));
        l_ligne_5.add(new Case(TypeCase.NEUTRE, 11));
        l_ligne_5.add(new Case(TypeCase.NEUTRE, 12));
        l_ligne_5.add(new Case(TypeCase.NEUTRE, 13));
        l_ligne_5.add(new Case(TypeCase.NEUTRE, 14));

        ArrayList<Case> l_ligne_6 = new ArrayList();

        l_ligne_6.add(new Case(TypeCase.NEUTRE, 0));
        l_ligne_6.add(new Case(TypeCase.BLEU_FONCE, 1));
        l_ligne_6.add(new Case(TypeCase.NEUTRE, 2));
        l_ligne_6.add(new Case(TypeCase.NEUTRE, 3));
        l_ligne_6.add(new Case(TypeCase.NEUTRE, 4));
        l_ligne_6.add(new Case(TypeCase.BLEU_FONCE, 5));
        l_ligne_6.add(new Case(TypeCase.NEUTRE, 6));
        l_ligne_6.add(new Case(TypeCase.NEUTRE, 7));
        l_ligne_6.add(new Case(TypeCase.NEUTRE, 8));
        l_ligne_6.add(new Case(TypeCase.BLEU_FONCE, 9));
        l_ligne_6.add(new Case(TypeCase.NEUTRE, 10));
        l_ligne_6.add(new Case(TypeCase.NEUTRE, 11));
        l_ligne_6.add(new Case(TypeCase.NEUTRE, 12));
        l_ligne_6.add(new Case(TypeCase.BLEU_FONCE, 13));
        l_ligne_6.add(new Case(TypeCase.NEUTRE, 14));

        ArrayList<Case> l_ligne_7 = new ArrayList();

        l_ligne_7.add(new Case(TypeCase.NEUTRE, 0));
        l_ligne_7.add(new Case(TypeCase.NEUTRE, 1));
        l_ligne_7.add(new Case(TypeCase.BLEU_CLAIRE, 2));
        l_ligne_7.add(new Case(TypeCase.NEUTRE, 3));
        l_ligne_7.add(new Case(TypeCase.NEUTRE, 4));
        l_ligne_7.add(new Case(TypeCase.NEUTRE, 5));
        l_ligne_7.add(new Case(TypeCase.BLEU_CLAIRE, 6));
        l_ligne_7.add(new Case(TypeCase.NEUTRE, 7));
        l_ligne_7.add(new Case(TypeCase.BLEU_CLAIRE, 8));
        l_ligne_7.add(new Case(TypeCase.NEUTRE, 9));
        l_ligne_7.add(new Case(TypeCase.NEUTRE, 10));
        l_ligne_7.add(new Case(TypeCase.NEUTRE, 11));
        l_ligne_7.add(new Case(TypeCase.BLEU_CLAIRE, 12));
        l_ligne_7.add(new Case(TypeCase.NEUTRE, 13));
        l_ligne_7.add(new Case(TypeCase.NEUTRE, 14));

        ArrayList<Case> l_ligne_8 = new ArrayList();

        l_ligne_8.add(new Case(TypeCase.ROUGE, 0));
        l_ligne_8.add(new Case(TypeCase.NEUTRE, 1));
        l_ligne_8.add(new Case(TypeCase.NEUTRE, 2));
        l_ligne_8.add(new Case(TypeCase.BLEU_CLAIRE, 3));
        l_ligne_8.add(new Case(TypeCase.NEUTRE, 4));
        l_ligne_8.add(new Case(TypeCase.NEUTRE, 5));
        l_ligne_8.add(new Case(TypeCase.NEUTRE, 6));
        l_ligne_8.add(new Case(TypeCase.CASE_ETOILE, 7));
        l_ligne_8.add(new Case(TypeCase.NEUTRE, 8));
        l_ligne_8.add(new Case(TypeCase.NEUTRE, 9));
        l_ligne_8.add(new Case(TypeCase.NEUTRE, 10));
        l_ligne_8.add(new Case(TypeCase.BLEU_CLAIRE, 11));
        l_ligne_8.add(new Case(TypeCase.NEUTRE, 12));
        l_ligne_8.add(new Case(TypeCase.NEUTRE, 13));
        l_ligne_8.add(new Case(TypeCase.ROUGE, 14));

        ArrayList<Case> l_ligne_9 = new ArrayList();

        l_ligne_9.add(new Case(TypeCase.NEUTRE, 0));
        l_ligne_9.add(new Case(TypeCase.NEUTRE, 1));
        l_ligne_9.add(new Case(TypeCase.BLEU_CLAIRE, 2));
        l_ligne_9.add(new Case(TypeCase.NEUTRE, 3));
        l_ligne_9.add(new Case(TypeCase.NEUTRE, 4));
        l_ligne_9.add(new Case(TypeCase.NEUTRE, 5));
        l_ligne_9.add(new Case(TypeCase.BLEU_CLAIRE, 6));
        l_ligne_9.add(new Case(TypeCase.NEUTRE, 7));
        l_ligne_9.add(new Case(TypeCase.BLEU_CLAIRE, 8));
        l_ligne_9.add(new Case(TypeCase.NEUTRE, 9));
        l_ligne_9.add(new Case(TypeCase.NEUTRE, 10));
        l_ligne_9.add(new Case(TypeCase.NEUTRE, 11));
        l_ligne_9.add(new Case(TypeCase.BLEU_CLAIRE, 12));
        l_ligne_9.add(new Case(TypeCase.NEUTRE, 13));
        l_ligne_9.add(new Case(TypeCase.NEUTRE, 14));

        ArrayList<Case> l_ligne_10 = new ArrayList();

        l_ligne_10.add(new Case(TypeCase.NEUTRE, 0));
        l_ligne_10.add(new Case(TypeCase.BLEU_FONCE, 1));
        l_ligne_10.add(new Case(TypeCase.NEUTRE, 2));
        l_ligne_10.add(new Case(TypeCase.NEUTRE, 3));
        l_ligne_10.add(new Case(TypeCase.NEUTRE, 4));
        l_ligne_10.add(new Case(TypeCase.BLEU_FONCE, 5));
        l_ligne_10.add(new Case(TypeCase.NEUTRE, 6));
        l_ligne_10.add(new Case(TypeCase.NEUTRE, 7));
        l_ligne_10.add(new Case(TypeCase.NEUTRE, 8));
        l_ligne_10.add(new Case(TypeCase.BLEU_FONCE, 9));
        l_ligne_10.add(new Case(TypeCase.NEUTRE, 10));
        l_ligne_10.add(new Case(TypeCase.NEUTRE, 11));
        l_ligne_10.add(new Case(TypeCase.NEUTRE, 12));
        l_ligne_10.add(new Case(TypeCase.BLEU_FONCE, 13));
        l_ligne_10.add(new Case(TypeCase.NEUTRE, 14));

        ArrayList<Case> l_ligne_11 = new ArrayList();

        l_ligne_11.add(new Case(TypeCase.NEUTRE, 0));
        l_ligne_11.add(new Case(TypeCase.NEUTRE, 1));
        l_ligne_11.add(new Case(TypeCase.NEUTRE, 2));
        l_ligne_11.add(new Case(TypeCase.NEUTRE, 3));
        l_ligne_11.add(new Case(TypeCase.ROSE, 4));
        l_ligne_11.add(new Case(TypeCase.NEUTRE, 5));
        l_ligne_11.add(new Case(TypeCase.NEUTRE, 6));
        l_ligne_11.add(new Case(TypeCase.NEUTRE, 7));
        l_ligne_11.add(new Case(TypeCase.NEUTRE, 8));
        l_ligne_11.add(new Case(TypeCase.NEUTRE, 9));
        l_ligne_11.add(new Case(TypeCase.ROSE, 10));
        l_ligne_11.add(new Case(TypeCase.NEUTRE, 11));
        l_ligne_11.add(new Case(TypeCase.NEUTRE, 12));
        l_ligne_11.add(new Case(TypeCase.NEUTRE, 13));
        l_ligne_11.add(new Case(TypeCase.NEUTRE, 14));

        ArrayList<Case> l_ligne_12 = new ArrayList();

        l_ligne_12.add(new Case(TypeCase.BLEU_CLAIRE, 0));
        l_ligne_12.add(new Case(TypeCase.NEUTRE, 1));
        l_ligne_12.add(new Case(TypeCase.NEUTRE, 2));
        l_ligne_12.add(new Case(TypeCase.ROSE, 3));
        l_ligne_12.add(new Case(TypeCase.NEUTRE, 4));
        l_ligne_12.add(new Case(TypeCase.NEUTRE, 5));
        l_ligne_12.add(new Case(TypeCase.NEUTRE, 6));
        l_ligne_12.add(new Case(TypeCase.BLEU_CLAIRE, 7));
        l_ligne_12.add(new Case(TypeCase.NEUTRE, 8));
        l_ligne_12.add(new Case(TypeCase.NEUTRE, 9));
        l_ligne_12.add(new Case(TypeCase.NEUTRE, 10));
        l_ligne_12.add(new Case(TypeCase.ROSE, 11));
        l_ligne_12.add(new Case(TypeCase.NEUTRE, 12));
        l_ligne_12.add(new Case(TypeCase.NEUTRE, 13));
        l_ligne_12.add(new Case(TypeCase.BLEU_CLAIRE, 14));

        ArrayList<Case> l_ligne_13 = new ArrayList();

        l_ligne_13.add(new Case(TypeCase.NEUTRE, 0));
        l_ligne_13.add(new Case(TypeCase.NEUTRE, 1));
        l_ligne_13.add(new Case(TypeCase.ROSE, 2));
        l_ligne_13.add(new Case(TypeCase.NEUTRE, 3));
        l_ligne_13.add(new Case(TypeCase.NEUTRE, 4));
        l_ligne_13.add(new Case(TypeCase.NEUTRE, 5));
        l_ligne_13.add(new Case(TypeCase.BLEU_CLAIRE, 6));
        l_ligne_13.add(new Case(TypeCase.NEUTRE, 7));
        l_ligne_13.add(new Case(TypeCase.BLEU_CLAIRE, 8));
        l_ligne_13.add(new Case(TypeCase.NEUTRE, 9));
        l_ligne_13.add(new Case(TypeCase.NEUTRE, 10));
        l_ligne_13.add(new Case(TypeCase.NEUTRE, 11));
        l_ligne_13.add(new Case(TypeCase.ROSE, 12));
        l_ligne_13.add(new Case(TypeCase.NEUTRE, 13));
        l_ligne_13.add(new Case(TypeCase.NEUTRE, 14));

        ArrayList<Case> l_ligne_14 = new ArrayList();

        l_ligne_14.add(new Case(TypeCase.NEUTRE, 0));
        l_ligne_14.add(new Case(TypeCase.ROSE, 1));
        l_ligne_14.add(new Case(TypeCase.NEUTRE, 2));
        l_ligne_14.add(new Case(TypeCase.NEUTRE, 3));
        l_ligne_14.add(new Case(TypeCase.NEUTRE, 4));
        l_ligne_14.add(new Case(TypeCase.BLEU_FONCE, 5));
        l_ligne_14.add(new Case(TypeCase.NEUTRE, 6));
        l_ligne_14.add(new Case(TypeCase.NEUTRE, 7));
        l_ligne_14.add(new Case(TypeCase.NEUTRE, 8));
        l_ligne_14.add(new Case(TypeCase.BLEU_FONCE, 9));
        l_ligne_14.add(new Case(TypeCase.NEUTRE, 10));
        l_ligne_14.add(new Case(TypeCase.NEUTRE, 11));
        l_ligne_14.add(new Case(TypeCase.NEUTRE, 12));
        l_ligne_14.add(new Case(TypeCase.ROSE, 13));
        l_ligne_14.add(new Case(TypeCase.NEUTRE, 14));

        ArrayList<Case> l_ligne_15 = new ArrayList();

        l_ligne_15.add(new Case(TypeCase.ROUGE, 0));
        l_ligne_15.add(new Case(TypeCase.NEUTRE, 1));
        l_ligne_15.add(new Case(TypeCase.NEUTRE, 2));
        l_ligne_15.add(new Case(TypeCase.BLEU_CLAIRE, 3));
        l_ligne_15.add(new Case(TypeCase.NEUTRE, 4));
        l_ligne_15.add(new Case(TypeCase.NEUTRE, 5));
        l_ligne_15.add(new Case(TypeCase.NEUTRE, 6));
        l_ligne_15.add(new Case(TypeCase.ROUGE, 7));
        l_ligne_15.add(new Case(TypeCase.NEUTRE, 8));
        l_ligne_15.add(new Case(TypeCase.NEUTRE, 9));
        l_ligne_15.add(new Case(TypeCase.NEUTRE, 10));
        l_ligne_15.add(new Case(TypeCase.BLEU_CLAIRE, 11));
        l_ligne_15.add(new Case(TypeCase.NEUTRE, 12));
        l_ligne_15.add(new Case(TypeCase.NEUTRE, 13));
        l_ligne_15.add(new Case(TypeCase.ROUGE, 14));

        this.addLigne(new Ligne(l_ligne_1));
        this.addLigne(new Ligne(l_ligne_2));
        this.addLigne(new Ligne(l_ligne_3));
        this.addLigne(new Ligne(l_ligne_4));
        this.addLigne(new Ligne(l_ligne_5));
        this.addLigne(new Ligne(l_ligne_6));
        this.addLigne(new Ligne(l_ligne_7));
        this.addLigne(new Ligne(l_ligne_8));
        this.addLigne(new Ligne(l_ligne_9));
        this.addLigne(new Ligne(l_ligne_10));
        this.addLigne(new Ligne(l_ligne_11));
        this.addLigne(new Ligne(l_ligne_12));
        this.addLigne(new Ligne(l_ligne_13));
        this.addLigne(new Ligne(l_ligne_14));
        this.addLigne(new Ligne(l_ligne_15));

        // On initialise le score à 0
        this.score = 0;
    }
}
