package fr.epsi.scrabble.fichier;

import fr.epsi.scrabble.joueur.Ordinateur;
import fr.epsi.scrabble.plateau.Grille;
import java.io.Serializable;

/**
 * @author benjamin
 */
public class Sauvegarde implements Serializable {

    Grille grille;
    Ordinateur ordinateur;

    /**
     * Constructeur
     *
     * @param grille
     * @param ordinateur
     */
    public Sauvegarde(Grille grille, Ordinateur ordinateur) {
        this.grille = grille;
        this.ordinateur = ordinateur;
    }

    /**
     * Charge une sauvegarde
     */
    public void charger() {
        Grille.newGrille(this.grille);
        Ordinateur.setInstance(this.ordinateur);
    }
}
