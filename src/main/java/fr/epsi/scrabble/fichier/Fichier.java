/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.scrabble.fichier;

import fr.epsi.scrabble.modele.Lettre;
import fr.epsi.scrabble.modele.Rack;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Florent
 */
public class Fichier {

    public static ArrayList<Rack> parseFichierRack(File fichier) {
        ArrayList<Lettre> listeLettre;
        ArrayList<Rack> listeRack = new ArrayList();

        try {
            Scanner scanner = new Scanner(fichier);
            while (scanner.hasNextLine()) {
                listeLettre = new ArrayList();
                String line = scanner.nextLine();
                for (int i = 0; i < line.length(); i++) {
                    listeLettre.add(Lettre.valueOf(String.valueOf(line.charAt(i)).toUpperCase()));
                }
                listeRack.add(new Rack(listeLettre));
            }

        } catch (Exception ex) {
        }

        return listeRack;

    }
}
