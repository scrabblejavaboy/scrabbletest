/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.scrabble.ihm;

import fr.epsi.scrabble.fichier.Fichier;
import fr.epsi.scrabble.fichier.Sauvegarde;
import fr.epsi.scrabble.joueur.Ordinateur;
import fr.epsi.scrabble.modele.Frame;
import fr.epsi.scrabble.modele.Lettre;
import fr.epsi.scrabble.modele.Rack;
import fr.epsi.scrabble.plateau.Case;
import fr.epsi.scrabble.plateau.Grille;
import fr.epsi.scrabble.plateau.Ligne;
import fr.epsi.scrabble.plateau.TypeCase;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author benjamin
 */
public class Main extends Frame {

    private JPanel panelGrille;
    private JPanel panelRack;
    private JPanel panelInfos;
    private JLabel lblScore;

    /**
     * Creates new form Main
     */
    public Main() throws Exception {
        super("Scrabble");
        initComponents();

        this.panelGrille = new JPanel();
        this.panelGrille.setPreferredSize(new Dimension(600, 600));

        this.initGrille();

        this.panelRack = new JPanel();
        this.panelRack.setPreferredSize(new Dimension(280, 40));

        this.initRack();

        this.panelInfos = new JPanel();
        this.panelInfos.setPreferredSize(new Dimension(280, 200));

        this.initInfos();

        JPanel content = new JPanel();
        content.setPreferredSize(new Dimension(300, 300));
        content.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridheight = 2;
        gbc.gridwidth = 1;

        content.add(this.panelGrille, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;

        content.add(this.panelRack, gbc);

        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;

        content.add(this.panelInfos, gbc);

        this.setContentPane(content);

        this.setIconImage(ImageIO.read(Main.class.getResourceAsStream("/icone.png")));

        this.setSize(891, 698);
        this.setVisible(true);
    }

    /**
     * Vide le rack
     */
    private void clearRack() {
        for (Component composant : this.panelRack.getComponents()) {
            ((JFormattedTextField) composant).setText("");
        }
    }

    /**
     * Met a jour la grille
     */
    private void updateGrille() {
        this.panelGrille.removeAll();

        this.initGrille();

        this.panelGrille.revalidate();
        this.panelGrille.repaint();
    }

    /**
     * Met à jour le score
     *
     * @param score
     */
    private void updateScore() {
        this.lblScore.setText(String.valueOf(Grille.getGrille().getScore()));
    }

    /**
     * Initiliase la grille
     */
    private void initGrille() {
        Grille l_plateau = Grille.getGrille();

        GridBagLayout l_layout = new GridBagLayout();
        GridBagConstraints l_gbc = new GridBagConstraints();

        this.panelGrille.setLayout(l_layout);

        int x = 0;
        int y = 0;

        for (Ligne _ligne : l_plateau.getLignes()) {
            for (Case _case : _ligne.getCases()) {
                JPanel l_panel = new JPanel();

                l_panel.setLayout(new BorderLayout());

                l_panel.setPreferredSize(new Dimension(20, 20));
                l_panel.setBorder(BorderFactory.createLineBorder(Color.black, 1));

                if (_case.getTypeCase().equals(TypeCase.CASE_ETOILE) && _case.getValue().equals("")) {
                    JLabel l_labelLettre = new JLabel();

                    l_labelLettre.setText("*");
                    l_labelLettre.setFont(new Font("Arial", Font.BOLD, 48));

                    l_panel.add(l_labelLettre, BorderLayout.CENTER);

                    l_panel.setBackground(_case.getTypeCase().getColor());
                } else if (!_case.getValue().equals("")) {
                    JLabel l_labelTop = new JLabel();
                    JLabel l_labelLettre = new JLabel();
                    JLabel l_labelValeur = new JLabel();

                    l_labelTop.setText(" ");
                    l_labelLettre.setText(_case.getValue());
                    l_labelValeur.setText(String.valueOf(_case.getLettre().getValeur()));

                    l_labelTop.setFont(new Font("Arial", Font.PLAIN, 8));
                    l_labelLettre.setFont(new Font("Arial", Font.BOLD, 20));
                    l_labelValeur.setFont(new Font("Arial", Font.PLAIN, 8));

                    l_labelLettre.setHorizontalAlignment(JLabel.CENTER);
                    l_labelValeur.setHorizontalAlignment(JLabel.RIGHT);

                    l_panel.add(l_labelTop, BorderLayout.NORTH);
                    l_panel.add(l_labelLettre, BorderLayout.CENTER);
                    l_panel.add(l_labelValeur, BorderLayout.SOUTH);

                    l_panel.setBackground(new Color(255, 230, 190));
                } else {
                    l_panel.setBackground(_case.getTypeCase().getColor());
                }

                l_gbc.gridx = x;
                l_gbc.gridy = y;
                l_gbc.fill = GridBagConstraints.BOTH;
                l_gbc.weightx = 1;
                l_gbc.weighty = 1;
                this.panelGrille.add(l_panel, l_gbc);
                x++;
            }
            x = 0;
            y++;
        }
    }

    /**
     * Initialise le rack
     *
     * @throws ParseException
     */
    private void initRack() throws ParseException {
        GridBagLayout l_layout = new GridBagLayout();
        GridBagConstraints l_gbc = new GridBagConstraints();

        this.panelRack.setLayout(l_layout);

        MaskFormatter masque = new MaskFormatter("U");

        for (int i = 0; i < 7; i++) {


            JFormattedTextField txtRack = new JFormattedTextField(masque);

            txtRack.setHorizontalAlignment(JTextField.CENTER);
            txtRack.setFont(new Font("Arial", Font.BOLD, 20));
            txtRack.setBorder(BorderFactory.createLineBorder(Color.black, 1));
            txtRack.setBackground(new Color(255, 230, 190));
            txtRack.setPreferredSize(new Dimension(20, 20));

            l_gbc.gridx = i;
            l_gbc.gridy = 0;
            l_gbc.fill = GridBagConstraints.BOTH;
            l_gbc.weightx = 1;
            l_gbc.weighty = 1;

            this.panelRack.add(txtRack, l_gbc);
        }
    }

    /**
     * Initialise le panneau d'infos
     */
    private void initInfos() {
        GridBagLayout l_layout = new GridBagLayout();
        GridBagConstraints l_gbc = new GridBagConstraints();

        this.panelInfos.setLayout(l_layout);
        this.panelInfos.setBorder(BorderFactory.createTitledBorder("Infos"));

        JButton boutonCalcul = new JButton();
        boutonCalcul.setText("Calculer le meilleur mot");
        boutonCalcul.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    boutonCalculActionPerformed(e);
                } catch (Exception ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        JButton boutonPlacer = new JButton();
        boutonPlacer.setText("Placer le mot");
        boutonPlacer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    boutonPlacerActionPerformed(e);
                } catch (Exception ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        JButton boutonAuto = new JButton();
        boutonAuto.setText("Mode automatique");
        boutonAuto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    boutonAutoActionPerformed(e);
                } catch (Exception ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        JLabel labelPoint = new JLabel();
        labelPoint.setText("Score : ");

        this.lblScore = new JLabel();
        this.lblScore.setText("0");

        l_gbc.gridx = 0;
        l_gbc.gridy = 0;
        l_gbc.fill = GridBagConstraints.BOTH;
        l_gbc.gridheight = 1;
        l_gbc.gridwidth = 1;
        l_gbc.weightx = 1;
        l_gbc.weighty = 1;

        this.panelInfos.add(labelPoint, l_gbc);

        l_gbc.gridx = 1;
        l_gbc.gridy = 0;
        this.panelInfos.add(this.lblScore, l_gbc);

        l_gbc.gridx = 0;
        l_gbc.gridy = 1;
        l_gbc.gridheight = 1;
        l_gbc.gridwidth = 2;

        this.panelInfos.add(boutonCalcul, l_gbc);

        l_gbc.gridx = 0;
        l_gbc.gridy = 2;

        this.panelInfos.add(boutonPlacer, l_gbc);

        l_gbc.gridx = 0;
        l_gbc.gridy = 3;

        this.panelInfos.add(boutonAuto, l_gbc);
    }

    /**
     * Lors du clic sur calculer
     *
     * @param evt
     * @throws Exception
     */
    private void boutonCalculActionPerformed(ActionEvent evt) throws Exception {
        Ordinateur ordi = Ordinateur.getInstance();
        String lettre;
        String message;

        if (ordi.getListeRack().isEmpty()) {
            Rack rack = new Rack();

            for (Component composant : this.panelRack.getComponents()) {
                if (!(((JFormattedTextField) composant).getText().isEmpty() || ((JFormattedTextField) composant).getText().equals(" "))) {
                    lettre = Lettre.stripAccent(((JFormattedTextField) composant).getText().charAt(0));

                    if (!lettre.isEmpty()) {
                        rack.addLettre(Lettre.valueOf(lettre));
                    }
                }
            }

            ArrayList<Rack> listeRack = new ArrayList();
            listeRack.add(rack);
            ordi.setListeRack(listeRack);
        }

        message = ordi.pasAPas();

        if (!message.isEmpty()) {
            JOptionPane.showMessageDialog(this, ordi.getMouvement().getMotString());
        }

    }

    /**
     * Lors du clic sur placer
     *
     * @param evt
     * @throws Exception
     */
    private void boutonPlacerActionPerformed(ActionEvent evt) throws Exception {
        Ordinateur ordi = Ordinateur.getInstance();
        if (ordi.getMouvement().getScore() > 0) {

            Grille.getGrille().placeMouvement(ordi.getMouvement());

            this.updateScore();
            this.updateGrille();
            this.clearRack();
        }
    }

    /**
     * Lors du clic sur le bouton
     *
     * @param evt
     * @throws Exception
     */
    private void boutonAutoActionPerformed(ActionEvent evt) throws Exception {
        Ordinateur ordi = Ordinateur.getInstance();
        ordi.automatique();

        this.updateGrille();
        this.updateScore();
        this.clearRack();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        MenuPartie = new javax.swing.JMenu();
        menuNouvellePartie = new javax.swing.JMenuItem();
        menuSauvegarder = new javax.swing.JMenuItem();
        menuCharger = new javax.swing.JMenuItem();
        menuQuitter = new javax.swing.JMenuItem();
        MenuLettre = new javax.swing.JMenu();
        menuChargerFichier = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Super Scrabble");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridLayout(15, 15));

        MenuPartie.setText("Partie");

        menuNouvellePartie.setText("Nouvelle partie");
        menuNouvellePartie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuNouvellePartieActionPerformed(evt);
            }
        });
        MenuPartie.add(menuNouvellePartie);

        menuSauvegarder.setText("Sauvegarder");
        menuSauvegarder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSauvegarderActionPerformed(evt);
            }
        });
        MenuPartie.add(menuSauvegarder);

        menuCharger.setText("Charger");
        menuCharger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChargerActionPerformed(evt);
            }
        });
        MenuPartie.add(menuCharger);

        menuQuitter.setText("Quitter");
        menuQuitter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuQuitterActionPerformed(evt);
            }
        });
        MenuPartie.add(menuQuitter);

        jMenuBar1.add(MenuPartie);
        MenuPartie.getAccessibleContext().setAccessibleDescription("");

        MenuLettre.setText("Lettre");

        menuChargerFichier.setText("Charger un fichier");
        menuChargerFichier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuChargerFichierActionPerformed(evt);
            }
        });
        MenuLettre.add(menuChargerFichier);

        jMenuBar1.add(MenuLettre);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Lors du clic sur le menu de nouvelle partie
     *
     * @param evt
     */
    private void menuNouvellePartieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuNouvellePartieActionPerformed
        try {
            Grille.newGrille();

            this.updateGrille();
            this.updateScore();
            this.clearRack();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_menuNouvellePartieActionPerformed

    /**
     * Lors du clic sur le bouton de sauvegarde
     *
     * @param evt
     */
    private void menuSauvegarderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSauvegarderActionPerformed
        try {
            JFileChooser fileChooser = new JFileChooser();

            FileNameExtensionFilter filter = new FileNameExtensionFilter("Sauvegarde Scrabble (*.ssb)", "ssb");

            fileChooser.setFileFilter(filter);

            if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                File fichier = fileChooser.getSelectedFile();

                FileOutputStream fichierSauvegarde = new FileOutputStream(fichier);
                try (ObjectOutputStream fichierObjet = new ObjectOutputStream(fichierSauvegarde)) {
                    fichierObjet.writeObject(new Sauvegarde(Grille.getGrille(), Ordinateur.getInstance()));
                }
            }
        } catch (HeadlessException | IOException ex) {
        }
    }//GEN-LAST:event_menuSauvegarderActionPerformed

    /**
     * Lors du clic sur le bouton de chargement
     *
     * @param evt
     */
    private void menuChargerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChargerActionPerformed
        try {
            JFileChooser fileChooser = new JFileChooser();

            FileNameExtensionFilter filter = new FileNameExtensionFilter("Sauvegarde Scrabble (*.ssb)", "ssb");

            fileChooser.setFileFilter(filter);

            if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                File fichier = fileChooser.getSelectedFile();

                FileInputStream fichierGrille = new FileInputStream(fichier);
                try (ObjectInputStream fichierObjet = new ObjectInputStream(fichierGrille)) {
                    Sauvegarde sauvegarde = (Sauvegarde) fichierObjet.readObject();
                    sauvegarde.charger();
                }
            }

            this.updateGrille();

            this.updateScore();
        } catch (HeadlessException | IOException | ClassNotFoundException ex) {
        }
    }//GEN-LAST:event_menuChargerActionPerformed

    /**
     * Lors du clic sur le bouton quitter
     *
     * @param evt
     */
    private void menuQuitterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuQuitterActionPerformed
        this.formWindowClosing(null);
    }//GEN-LAST:event_menuQuitterActionPerformed

    /**
     * Lors de la fermeture de la fenetre
     *
     * @param evt
     */
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        int responseQuit = JOptionPane.showConfirmDialog(this, "Etes-vous sur de vouloir quitter notre super Scrabble ?", "Quitter", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);

        if (responseQuit == JOptionPane.YES_OPTION) {
            System.exit(0);
        }





    }//GEN-LAST:event_formWindowClosing

    /**
     * Lors du clic sur le bouton Charger Fichier
     *
     */
    private void menuChargerFichierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuChargerFichierActionPerformed
        try {
            JFileChooser fileChooser = new JFileChooser();

            FileNameExtensionFilter filter = new FileNameExtensionFilter("Fichier texte", "txt");

            fileChooser.setFileFilter(filter);

            if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                File fichier = fileChooser.getSelectedFile();
                Ordinateur.getInstance().setListeRack(Fichier.parseFichierRack(fichier));

            }
        } catch (Exception ex) {
        }
    }//GEN-LAST:event_menuChargerFichierActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu MenuLettre;
    private javax.swing.JMenu MenuPartie;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem menuCharger;
    private javax.swing.JMenuItem menuChargerFichier;
    private javax.swing.JMenuItem menuNouvellePartie;
    private javax.swing.JMenuItem menuQuitter;
    private javax.swing.JMenuItem menuSauvegarder;
    // End of variables declaration//GEN-END:variables
}
