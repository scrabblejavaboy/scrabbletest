/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.scrabble.joueur;

import fr.epsi.scrabble.plateau.Grille;
import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author Benjamin
 */
public class Ancre {

    /**
     * Retourne la liste des ancres possibles
     *
     * @return
     */
    public static ArrayList<Point> getAncre(boolean isHorizontal) throws Exception {
        ArrayList<Point> listePoint = new ArrayList<>();
        Grille grille = Grille.getGrille();
        if (grille.possedeLettre()) {

            for (int ligne = 0; ligne < 15; ligne++) {
                for (int colonne = 0; colonne < 15; colonne++) {
                    if (grille.getLettre(colonne, ligne) != null) {
                        if (!isHorizontal && (grille.getLettre(colonne, ligne - 1) == null && grille.getLettre(colonne, ligne + 1) == null)) {
                            listePoint.add(new Point(colonne, ligne));
                        } else if (isHorizontal && (grille.getLettre(colonne - 1, ligne) == null && grille.getLettre(colonne + 1, ligne) == null)) {
                            listePoint.add(new Point(colonne, ligne));
                        }
                    }
                }
            }

        } else {
            listePoint.add(new Point(7, 7));
        }
        return listePoint;
    }
}
