package fr.epsi.scrabble.joueur;

import fr.epsi.scrabble.modele.Rack;
import fr.epsi.scrabble.plateau.Grille;
import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author benjamin
 */
public class Ordinateur implements Serializable{

    private static Ordinateur instance = null;
    private Mouvement mouvement;
    private ArrayList<Rack> listeRack;

    /**
     * Constructeur
     */
    private Ordinateur() {
        this.listeRack = new ArrayList();
        this.mouvement = new Mouvement();
    }
    
    /**
     * Singleton
     *
     * @return
     */
    public static Ordinateur getInstance() {
        if (instance == null) {
            instance = new Ordinateur();
        }
        return instance;
    }
    
    /**
     * Définis l'instance
     * @param ordi 
     */
    public static void setInstance(Ordinateur ordi)
    {
        instance = ordi;
    }

    /**
     * Calcul le meilleur mot et son emplacement
     *
     * @param rack
     * @throws Exception
     */
    public void calculMeilleurMot(Rack rack) throws Exception {
        int point = 0;
        int maxPoint = 0;
        Mouvement mouvement;
        Mouvement meilleurMouvement = new Mouvement();

        // Pour chaque ancre vertical
        for (Point p : Ancre.getAncre(true)) {
            mouvement = new Mouvement(p, Direction.OUEST, rack);
            point = mouvement.getScore();

            if (point > maxPoint) {
                maxPoint = point;
                meilleurMouvement = mouvement;
            }

            mouvement = new Mouvement(p, Direction.EST, rack);
            point = mouvement.getScore();

            if (point > maxPoint) {
                maxPoint = point;
                meilleurMouvement = mouvement;
            }
        }

        // Pour chaque ancre horizontal
        for (Point p : Ancre.getAncre(false)) {
            mouvement = new Mouvement(p, Direction.NORD, rack);
            point = mouvement.getScore();

            if (point > maxPoint) {
                maxPoint = point;
                meilleurMouvement = mouvement;
            }

            mouvement = new Mouvement(p, Direction.SUD, rack);
            point = mouvement.getScore();

            if (point > maxPoint) {
                maxPoint = point;
                meilleurMouvement = mouvement;
            }
        }

        this.mouvement = meilleurMouvement;
    }

    /**
     * Retourne le mouvement
     *
     * @return
     */
    public Mouvement getMouvement() {
        return mouvement;
    }

    /**
     * Retourne la liste des racks
     *
     * @return
     */
    public ArrayList<Rack> getListeRack() {
        return listeRack;
    }

    /**
     * Définis la liste des racks
     *
     * @param listeRack
     */
    public void setListeRack(ArrayList<Rack> listeRack) {
        this.listeRack = listeRack;
    }

    /**
     * Mode pas à pas
     *
     * @return
     * @throws Exception
     */
    public String pasAPas() throws Exception {
        String message = "";

        if (!this.listeRack.isEmpty()) {
            this.calculMeilleurMot(this.listeRack.get(0));

            this.listeRack.remove(0);

            if (this.mouvement.getScore() > 0) {
                message = this.mouvement.getMotString();
            }
        }

        return message;
    }

    /**
     * Mode automatique
     */
    public void automatique() throws Exception {
        for (Rack rack : this.listeRack) {
            this.calculMeilleurMot(rack);

            if (this.mouvement.getScore() > 0) {
                Grille.getGrille().placeMouvement(this.mouvement);
            }
        }

        this.listeRack.clear();
    }
}
