/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.scrabble.joueur;

import fr.epsi.scrabble.dictionnaire.Dictionnaire;
import static fr.epsi.scrabble.joueur.Direction.EST;
import static fr.epsi.scrabble.joueur.Direction.NORD;
import static fr.epsi.scrabble.joueur.Direction.OUEST;
import static fr.epsi.scrabble.joueur.Direction.SUD;
import fr.epsi.scrabble.modele.Lettre;
import fr.epsi.scrabble.modele.Mot;
import fr.epsi.scrabble.modele.Mots;
import fr.epsi.scrabble.modele.Rack;
import fr.epsi.scrabble.plateau.Case;
import fr.epsi.scrabble.plateau.Grille;
import fr.epsi.scrabble.plateau.TypeMultiplieur;
import java.awt.Point;
import java.io.Serializable;

/**
 *
 * @author Benjamin
 */
public class Mouvement implements Serializable{

    private int score;
    private Point ancre;
    private Direction direction;
    private Mot mot;

    /**
     * Constructeur vide
     */
    public Mouvement() {
    }

    /**
     * Constructeur
     *
     * @param ancre
     * @param direction
     * @param rack
     * @throws Exception
     */
    public Mouvement(Point ancre, Direction direction, Rack rack) throws Exception {
        this.ancre = ancre;
        this.direction = direction;
        this.mot = this.calculMot(rack);
        this.score = this.getPoint();
    }

    /**
     * Calcul le meilleur mot pour le mouvement
     *
     * @param rack
     * @return
     * @throws Exception
     */
    private Mot calculMot(Rack rack) throws Exception {
        Dictionnaire dico = Dictionnaire.getInstance();
        Grille grille = Grille.getGrille();
        Mot meilleurMot = new Mot("");
        Mots listesMots;
        int pointMeilleurMot = 0;
        int pointMot = 0;

        if (grille.getLettre(this.ancre.x, this.ancre.y) != null) {
            listesMots = dico.rechercheMots(rack, grille.getLettre(this.ancre.x, this.ancre.y), this.direction);
        } else {
            listesMots = dico.rechercheMots(rack, this.direction);
        }

        for (Mot mot : listesMots.getMots().values()) {
            pointMot = this.getPoint(mot);

            if (pointMot > pointMeilleurMot) {
                meilleurMot = mot;
                pointMeilleurMot = pointMot;
            }
        }

        return meilleurMot;
    }

    /**
     * Retourne le score du mouvement
     *
     * @return
     */
    public int getScore() {
        return this.score;
    }

    /**
     * Calcul le nombre de point du mot du mouvement
     *
     * @return
     * @throws Exception
     */
    private int getPoint() throws Exception {
        return this.getPoint(this.mot);
    }

    /**
     * Calcul le nombre de point du mot
     *
     * @param mot
     * @return
     * @throws Exception
     */
    private int getPoint(Mot mot) throws Exception {
        Grille grille = Grille.getGrille();
        Point p = this.ancre;
        int point = 0;
        int motMultiplieur = 1;

        for (Lettre lettre : mot.getLettres()) {
            if (p.x >= 0 && p.y >= 0 && p.x < 14 && p.y < 14 && this.motAdjacent(p, lettre)) {
                Case caseGrille = grille.getCase(p.x, p.y);

                if (caseGrille.getLettre() != null && !caseGrille.getLettre().equals(lettre)) {
                    point = 0;
                    break;
                }

                if (caseGrille.getTypeCase().getTypeMultiplieur().equals(TypeMultiplieur.WORD)) {
                    motMultiplieur *= caseGrille.getTypeCase().getMultiplieur();
                }

                if (caseGrille.getTypeCase().getTypeMultiplieur().equals(TypeMultiplieur.LETTER)) {
                    point += caseGrille.getTypeCase().getMultiplieur() * lettre.getValeur();
                } else {
                    point += lettre.getValeur();
                }

                p = this.getNextPoint(p);
            } else {
                point = 0;
                break;
            }
        }

        // On vérifie qu'il n'y a pas de lettre après le mot
        if (p.x >= 0 && p.y >= 0 && p.x < 14 && p.y < 14) {
            if (grille.getCase(p.x, p.y).getLettre() != null) {
                point = 0;
            }
        }

        point *= motMultiplieur;

        return point;
    }

    /**
     * Retourne l'ancre
     *
     * @return
     */
    public Point getAncre() {
        return this.ancre;
    }

    /**
     * Retourne le prochain point
     *
     * @param p
     * @return
     */
    public Point getNextPoint(Point p) {
        return this.getNextPoint(p, this.direction);
    }

    /**
     * Retourne le point suivant
     *
     * @param p
     * @param d
     * @return
     */
    public Point getNextPoint(Point p, Direction d) {
        Point nextPoint;

        switch (d) {
            case NORD:
                nextPoint = new Point(p.x, p.y - 1);
                break;
            case SUD:
                nextPoint = new Point(p.x, p.y + 1);
                break;
            case OUEST:
                nextPoint = new Point(p.x - 1, p.y);
                break;
            case EST:
                nextPoint = new Point(p.x + 1, p.y);
                break;
            default:
                nextPoint = p;
        }

        return nextPoint;
    }

    /**
     * Retourne si un mot adjacent créé est possible
     *
     * @param p
     * @return
     * @throws Exception
     */
    public boolean motAdjacent(Point p, Lettre lettre) throws Exception {
        Point pointAvant;
        Point pointApres;
        Direction directionAvant;
        Direction directionApres;
        Mot mot = new Mot(lettre.toString());
        Grille grille = Grille.getGrille();
        Dictionnaire dico = Dictionnaire.getInstance();

        if (this.direction.equals(Direction.NORD) || this.direction.equals(Direction.SUD)) {
            pointAvant = this.getNextPoint(p, Direction.OUEST);
            pointApres = this.getNextPoint(p, Direction.EST);

            directionAvant = Direction.OUEST;
            directionApres = Direction.EST;

        } else {
            pointAvant = this.getNextPoint(p, Direction.NORD);
            pointApres = this.getNextPoint(p, Direction.SUD);

            directionAvant = Direction.NORD;
            directionApres = Direction.SUD;
        }

        while ((pointAvant.x >= 0 && pointAvant.y >= 0 && pointAvant.x < 14 && pointAvant.y < 14) && (grille.getCase(pointAvant.x, pointAvant.y).getLettre() != null)) {

            Case caseGrille = grille.getCase(pointAvant.x, pointAvant.y);

            mot = new Mot(mot.toString() + caseGrille.getLettre().toString());

            pointAvant = this.getNextPoint(pointAvant, directionAvant);
        }

        while ((pointApres.x >= 0 && pointApres.y >= 0 && pointApres.x < 14 && pointApres.y < 14) && (grille.getCase(pointApres.x, pointApres.y).getLettre() != null)) {

            Case caseGrille = grille.getCase(pointApres.x, pointApres.y);

            mot = new Mot(caseGrille.getLettre().toString() + mot.toString());

            pointApres = this.getNextPoint(pointApres, directionApres);
        }

        return mot.lenght() > 0 && (dico.contains(mot) || dico.contains(new Mot(mot.toStringInverse())));
    }

    /**
     * Retourne le mot
     *
     * @return
     */
    public Mot getMot() {
        return this.mot;
    }

    /**
     * Retourne le mot
     *
     * @return
     */
    public String getMotString() {
        String mot = "";
        if (this.mot != null) {
            if (this.getDirection().equals(Direction.OUEST) || this.getDirection().equals(Direction.NORD)) {
                mot = this.mot.toStringInverse();
            } else {
                mot = this.mot.toString();
            }
        }
        return mot;
    }

    /**
     * Retourne la direction
     *
     * @return
     */
    public Direction getDirection() {
        return direction;
    }
}
