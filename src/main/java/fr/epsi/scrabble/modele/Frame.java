package fr.epsi.scrabble.modele;

import java.awt.Component;
import javax.swing.JFrame;

/**
 * @author benjamin
 */
public class Frame extends JFrame {

    private JFrame m_frame;

    public Frame(String _nomFenetre) {
        super();
        this.buildInterface(_nomFenetre);
    }

    private void buildInterface(String _nomFenetre) {
        this.setTitle(_nomFenetre);
        this.setSize(320, 240);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.m_frame = new JFrame();
    }

    protected void addComponentToFrame(Component _composant) throws Exception {
        try {
            this.m_frame.add(_composant);
        } catch (Exception e) {
            throw new Exception(e.getMessage() + ", " + this.getClass().getName());
        }
    }
}
