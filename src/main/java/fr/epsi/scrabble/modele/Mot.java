package fr.epsi.scrabble.modele;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author benjamin
 */
public class Mot implements Serializable{

    private ArrayList<Lettre> lettres;

    /**
     * Constructeur de la classe
     *
     * @param lettres
     */
    public Mot(ArrayList<Lettre> lettres) {
        this.lettres = lettres;
    }

    /**
     * Constructeur de la classe
     *
     * @param mot
     */
    public Mot(String mot) {
        String lettre;

        this.lettres = new ArrayList<>();

        char[] lettreMot = mot.toCharArray();

        for (int i = 0; i < lettreMot.length; i++) {
            lettre = Lettre.stripAccent(lettreMot[i]);

            if (!lettre.isEmpty()) {
                this.lettres.add(Lettre.valueOf(lettre));
            }
        }
    }

    /**
     * Retourne la liste des lettres du mot
     *
     * @return
     */
    public ArrayList<Lettre> getLettres() {
        return lettres;
    }

    /**
     * Retourne la longueur du mot
     *
     * @return
     */
    public int lenght() {
        return this.lettres.size();
    }

    /**
     * Retourne le mot sous forme de String
     *
     * @return
     */
    @Override
    public String toString() {
        String mot = "";

        if (this.lettres != null) {
            for (Lettre lettre : this.lettres) {
                mot += lettre.toString();
            }
        }

        return mot;
    }
    
    public String toStringInverse()
    {
        return new StringBuilder(this.toString()).reverse().toString();
    }
}