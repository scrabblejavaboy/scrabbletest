/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.scrabble.modele;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Benjamin
 */
public class Rack implements Cloneable, Serializable {

    private ArrayList<Lettre> lettres;

    /**
     * Constructeur
     */
    public Rack() {
        this.lettres = new ArrayList<>();
    }

    /**
     * Constructeur
     *
     * @param lettres
     */
    public Rack(ArrayList<Lettre> lettres) {
        this.lettres = lettres;
    }

    /**
     * Ajoute une lettre
     *
     * @param lettre
     */
    public void addLettre(Lettre lettre) {
        this.lettres.add(this.lettres.size(), lettre);
    }

    /**
     * Retourne la liste des lettres
     *
     * @return
     */
    public ArrayList<Lettre> getLettres() {
        return lettres;
    }

    /**
     * Supprime une lettre du rack
     *
     * @param index
     */
    public void supprimeLettre(int index) {
        this.lettres.remove(index);
    }

    /**
     * Supprime une lettre du rack
     *
     * @param lettre
     */
    public void supprimeLettre(Lettre lettre) {
        int index = this.lettres.lastIndexOf(lettre);
        this.supprimeLettre(index);
    }

    /**
     * Retourne le nombre de lettre du rack
     *
     * @return
     */
    public int lenght() {
        return this.lettres.size();
    }

    /**
     * Retourne vrai si la lettre est dans le rack
     *
     * @param lettre
     * @return
     */
    public boolean contains(Lettre lettre) {
        return this.lettres.contains(lettre);
    }

    /**
     * Permet de cloner le rack
     *
     * @return
     */
    @Override
    public Object clone() {
        Rack o = null;
        try {
            o = new Rack((ArrayList<Lettre>) this.lettres.clone());
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }

        return o;
    }
}
