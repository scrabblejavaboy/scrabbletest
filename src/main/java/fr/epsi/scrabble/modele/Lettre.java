/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.scrabble.modele;

import java.text.Normalizer;
import java.util.regex.Pattern;

/**
 *
 * @author benjamin
 */
public enum Lettre {

    E(1),
    A(1),
    I(1),
    N(1),
    O(1),
    R(1),
    S(1),
    T(1),
    U(1),
    L(1),
    D(2),
    M(2),
    G(2),
    B(3),
    C(3),
    P(3),
    F(4),
    H(4),
    V(4),
    J(8),
    Q(8),
    K(10),
    W(10),
    X(10),
    Y(10),
    Z(10),
    RACINE(0);
    private int value;

    /**
     * Constructeur privé
     *
     * @param _valeur
     */
    private Lettre(int _valeur) {
        this.value = _valeur;
    }

    /**
     * Retoourne la valeur de la lettre
     *
     * @return
     */
    public int getValeur() {
        return this.value;
    }

    /**
     * Retourne le nom de la lettre
     *
     * @return
     */
    @Override
    public String toString() {
        String name;

        if (this.equals(Lettre.RACINE)) {
            name = "";
        } else {
            name = super.toString();
        }

        return name;
    }

    /**
     * Retourne la lettre en Char
     *
     * @return
     */
    public char toChar() {
        return this.name().charAt(0);
    }

    /**
     * Retourne l'enum d'une lettre
     *
     * @param value
     * @return
     */
    public static String stripAccent(char value) {
        String unChar = String.valueOf(value).toLowerCase();

        unChar = Normalizer.normalize(unChar, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");

        unChar = pattern.matcher(unChar).replaceAll("");

        return unChar.replaceAll("[^A-Za-z]+", "").toUpperCase();
    }
}
