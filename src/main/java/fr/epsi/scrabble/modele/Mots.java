/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.epsi.scrabble.modele;
import java.util.HashMap;

/**
 *
 * @author Florent
 */
public class Mots {
    
    private HashMap<Integer, Mot> hm;
    
    public Mots(){
        hm = new HashMap();
    }
    
    public void addMot(Integer clef, Mot mot){
        hm.put(clef, mot);
    }
    
    /**
     * Methode pour ajouter un mot sans saisir la clef
     * Benjamin
     * 
     * @param mot 
     */
    public void addMot(Mot mot)
    {
        this.addMot(this.hm.size(),mot);
    }
    
    /**
     * Supprime un mot de la liste
     * 
     * @param mot 
     */
    public void supprimerMot(Object mot){
        hm.remove(mot);
    }
    
    /**
     * Retourne la liste des mots
     * Benjamin
     * 
     * @return Mots
     */
    public HashMap<Integer,Mot> getMots()
    {
        return this.hm;
    }
}
