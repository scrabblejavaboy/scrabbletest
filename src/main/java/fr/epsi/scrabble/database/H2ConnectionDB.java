package fr.epsi.scrabble.database;

import fr.epsi.scrabble.modele.Mot;
import fr.epsi.scrabble.modele.Mots;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class H2ConnectionDB {

    ResultSet rs;
    Connection conn;
    Statement stat;

    public H2ConnectionDB() throws Exception {
        Class.forName("org.h2.Driver");
        this.conn = DriverManager.getConnection("jdbc:h2:file:scrabble.mv.db", "scrabble", "");
        this.stat = conn.createStatement();

    }

    public Mots selectAllMots() throws Exception {
        this.rs = stat.executeQuery("select * from DICTIONNAIRE");
        Mots listeMots = new Mots();
        while (rs.next()) {
            Mot nouveauMot = new Mot(rs.getString(2));
            
            if (nouveauMot.lenght() > 0) {
                listeMots.addMot(rs.getInt(1), nouveauMot);
            }
        }
        return listeMots;
    }
}
