/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.scrabble.dictionnaire;

import fr.epsi.scrabble.modele.Lettre;
import java.io.Serializable;
import java.util.Collection;
import java.util.TreeMap;

/**
 *
 * @author Benjamin
 */
public class Noeud implements Comparable<Noeud>,Serializable{

    public static Lettre racine = Lettre.RACINE;
    private Lettre contenu;
    private boolean termine;
    public TreeMap<Lettre, Noeud> enfants = new TreeMap<>();

    /**
     * Retourne le contenu du noeud
     * @return 
     */
    public Lettre getContenu() {
        return contenu;
    }

    /**
     * Retourne si le noeud est complet
     * @return 
     */
    public boolean isTermine() {
        return termine;
    }

    /**
     * Defini si le noeud est terminé
     * @param termine 
     */
    public void setTermine(boolean termine) {
        this.termine = termine;
    }

    /**
     * Constructeur
     * @param contenu 
     */
    public Noeud(Lettre contenu) {
        this.contenu = contenu;
        this.termine = false;
    }
    
    /**
     * Retourne la liste des clés
     * @return Caractere
     */
    public Collection<Lettre> getCles(){
        return this.enfants.keySet();
    }

    /**
     * Retourne la liste des enfants
     * @return Noeud
     */
    public Collection<Noeud> getEnfants() {
        return this.enfants.values();
    }
    
    /**
     * Retourne si le noeud est le dernier noeud d'une chaine
     * @param contenu
     * @return Faux si dernier
     */
    public boolean hasEnfant(Lettre contenu)
    {
        return this.enfants.containsKey(contenu);
    }
    
    /**
     * Retourne un enfant
     * @param contenu
     * @return Noeud
     */
    public Noeud getEnfant(Lettre contenu)
    {
        return this.enfants.get(contenu);
    }
    
    /**
     * Ajoute un enfant
     * @param contenu 
     */
    public void addEnfant(Lettre contenu)
    {
        this.enfants.put(contenu, new Noeud(contenu));
    }
    
    /**
     * Surcharge de la méthode equals
     * @param o
     * @return 
     */
    @Override
    public boolean equals(Object o)
    {
        return o instanceof Noeud && this.contenu == ((Noeud) o).getContenu();
    }
    
    /**
     * Surcharge de la méthode compareTo
     * @param o
     * @return 
     */
    @Override
    public int compareTo(Noeud o) {
        return this.contenu.toChar() - o.contenu.toChar();
    }
    
}
