/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.scrabble.dictionnaire;

import fr.epsi.scrabble.joueur.Direction;
import fr.epsi.scrabble.modele.Lettre;
import fr.epsi.scrabble.modele.Mot;
import fr.epsi.scrabble.modele.Mots;
import fr.epsi.scrabble.modele.Rack;
import java.io.Serializable;

/**
 * Adaptation du GADDAG
 *
 * @author Benjamin
 */
public class Dictionnaire extends Arbre implements Serializable {

    private static Dictionnaire instance = null;

    /**
     * Singleton
     *
     * @return
     */
    public static Dictionnaire getInstance() {
        if (instance == null) {
            instance = new Dictionnaire();
        }
        return instance;
    }

    /**
     * Constructeur
     */
    private Dictionnaire() {
        super();
    }

    /**
     * Recherche les mots qui peuvent être créé
     *
     * @param rack
     * @return
     */
    public Mots rechercheMots(Rack rack, Direction direction) {
        Mots listesMots = new Mots();

        this.rechercheMotsRecursivement(listesMots, new Mot(""), rack, this.racine, direction);

        return listesMots;
    }

    /**
     * Recherche les mots qui peuvent être créé avec une lettre obligatoirement
     * comprise
     *
     * @param rack
     * @param lettrePresente
     * @return
     */
    public Mots rechercheMots(Rack rack, Lettre lettrePresente, Direction direction) {
        Mots listesMots = new Mots();

        if (!lettrePresente.equals(Lettre.RACINE)) {
            this.rechercheMotsRecursivement(listesMots, new Mot(""), rack, lettrePresente, this.racine, direction);
        }

        return listesMots;
    }

    /**
     * Retourne la liste des mots possible
     *
     * @param listesMots
     * @param mot
     * @param rack
     * @param noeudCourant
     */
    public void rechercheMotsRecursivement(Mots listesMots, Mot mot, Rack rack, Noeud noeudCourant, Direction direction) {
        Mot l_mot = new Mot((direction.equals(Direction.SUD) || direction.equals(Direction.OUEST) ? noeudCourant.getContenu().toString() + mot.toString() : mot.toString() + noeudCourant.getContenu().toString()));

        if (noeudCourant.isTermine()) {
            listesMots.addMot(l_mot);
        }
        for (Lettre lettre : noeudCourant.getCles()) {
            if (rack.contains(lettre)) {
                Rack rackTemp = (Rack) rack.clone();
                rackTemp.supprimeLettre(lettre);
                this.rechercheMotsRecursivement(listesMots, l_mot, rackTemp, noeudCourant.getEnfant(lettre), direction);
            }

        }
    }

    /**
     * Retourne la liste des mots possible avec une lettre comprise
     *
     * @param listesMots
     * @param mot
     * @param rack
     * @param lettrePresente
     * @param noeudCourant
     * @param direction
     */
    public void rechercheMotsRecursivement(Mots listesMots, Mot mot, Rack rack, Lettre lettrePresente, Noeud noeudCourant, Direction direction) {
        Noeud noeudLettrePresente = noeudCourant.getEnfant(lettrePresente);

        if (noeudLettrePresente != null) {
            mot = new Mot((direction.equals(Direction.NORD) || direction.equals(Direction.OUEST) ? lettrePresente.toString() + mot.toString() : mot.toString() + lettrePresente.toString()));

            if (noeudLettrePresente.isTermine() && mot.lenght() > 1) {
                listesMots.addMot(mot);
            }
            for (Lettre lettre : noeudLettrePresente.getCles()) {
                if (rack.contains(lettre)) {
                    Rack rackTemp = (Rack) rack.clone();
                    rackTemp.supprimeLettre(lettre);
                    this.rechercheMotsRecursivement(listesMots, mot, rackTemp, lettre, noeudLettrePresente, direction);
                }
            }

        }
    }
}
