/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.scrabble.dictionnaire;

import fr.epsi.scrabble.modele.Lettre;
import fr.epsi.scrabble.modele.Mot;
import fr.epsi.scrabble.modele.Mots;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Benjamin
 */
public class Arbre implements Serializable{

    protected Noeud racine;
    protected long nombreNoeud;
    protected int nombreMot;
    protected int profondeurMax;

    /**
     * Retourne le noeud racine
     *
     * @return Noeud
     */
    public Noeud getRacine() {
        return racine;
    }

    /**
     * Retourne le nombre de noeud
     *
     * @return Nombre de noeud
     */
    public long getNombreNoeud() {
        return nombreNoeud;
    }

    /**
     * Retourne le nombre de mot
     *
     * @return Nombre de mot
     */
    public int getNombreMot() {
        return nombreMot;
    }

    /**
     * Retourne la profondeur max de l'arbre
     *
     * @return
     */
    public int getProfondeurMax() {
        return profondeurMax;
    }

    /**
     * Constructeur
     */
    public Arbre() {
        this.racine = new Noeud(Noeud.racine);
        this.nombreNoeud = 1;
        this.nombreMot = 0;
        this.profondeurMax = 0;
    }

    /**
     * Ajoute une liste de mots
     *
     * @param mots
     */
    public void addMots(Mots mots) {
        for (Mot _mot : mots.getMots().values()) {
            this.add(_mot);
        }
    }

    /**
     * Ajoute un mot
     *
     * @param mot
     */
    public void add(Mot mot) {
        if (mot.lenght() > 0) {
            Noeud noeudCourant = this.racine;

            for (Lettre lettre : mot.getLettres()) {
                if (!noeudCourant.hasEnfant(lettre)) {
                    noeudCourant.addEnfant(lettre);
                    this.nombreNoeud++;
                }
                noeudCourant = noeudCourant.getEnfant(lettre);
            }

            // Le mot est terminé on passe le flag a vrai
            noeudCourant.setTermine(true);

            //Si le mot est plus profond que l'arbre on change la profondeur
            if (mot.lenght() > this.profondeurMax) {
                this.profondeurMax = mot.lenght();
            }
            // On incrmeente le nombre de mot
            this.nombreMot++;
        }
    }

    /**
     * Retourne vrai si l'arbre contient le mot
     *
     * @param mot
     * @return
     */
    public boolean contains(Mot mot) {
        Noeud noeud = this.racine;

        for (Lettre lettre : mot.getLettres()) {
            if (noeud.hasEnfant(lettre)) {
                noeud = noeud.getEnfant(lettre);
            } else {
                return false;
            }
        }

        return noeud.isTermine();
    }

    /**
     * Retourne le dernier noeud d'un prefixe
     *
     * @param prefixe
     * @return
     */
    public Noeud find(Mot prefixe) {
        Noeud noeud = this.racine;

        for (Lettre lettre : prefixe.getLettres()) {
            if (noeud.hasEnfant(lettre)) {
                noeud = noeud.getEnfant(lettre);
            } else {
                return null;
            }
        }

        return noeud;
    }

    /**
     * Retourne la liste de tout les mots possible
     *
     * @return
     */
    public Mots getAllMots() {
        Mots listeMots = new Mots();

        this.getMots(new Mot((ArrayList<Lettre>) null), this.racine, listeMots);

        return listeMots;
    }

    /**
     * Retourne la liste des mots a partir d'un mot et d'un noeud courant
     *
     * @param mot
     * @param noeudCourant
     * @param listesMots
     */
    public void getMots(Mot mot, Noeud noeudCourant, Mots listesMots) {
        if (noeudCourant.isTermine()) {
            listesMots.addMot(mot);
        }

        if (noeudCourant.getEnfants() != null) {
            for (Noeud noeud : noeudCourant.getEnfants()) {
                this.getMots(new Mot(mot.toString() + noeud.getContenu().toString()), noeud, listesMots);
            }
        }
    }
}
